{
  local argoApiVersion = "argoproj.io/v1alpha1",

  onlyApplications(resources):
    std.filter(function (r) r.apiVersion == argoApiVersion && r.kind == "Application", resources),

  withoutApplications(resources):
    std.filter(function (r) r.apiVersion != argoApiVersion, resources),
}
