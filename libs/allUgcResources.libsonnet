local solvers = (import '../components/tls/solvers.libsonnet');
local externalDnsProviderConfig = {
  cloudflare: {
    secretName: "cloudflare",
  }
};

function(kubernetesServer, namePrefix, domain, internalDomain, letsencryptServer, linkerdReplicas, prometheusReplicas, lokiReplicas, observabilityRevision, discoursePgConfiguration, smtpConfiguration, installDiscourse, discourseAdminUser, enableObservabilityStack)
  local common = (import 'common.libsonnet')(kubernetesServer, namePrefix);

  (import '../components/bootstrap.libsonnet')(kubernetesServer, namePrefix) +
  std.objectValues((import '../components/cert-manager.libsonnet')(kubernetesServer, namePrefix)) +
  std.objectValues((import '../components/linkerd.libsonnet')(kubernetesServer, namePrefix, domain, internalDomain + domain, true, linkerdReplicas, enableObservabilityStack)) +
  std.objectValues((import '../components/contour.libsonnet')(kubernetesServer, namePrefix, domain, 'dev.' + domain, internalDomain + domain, letsencryptServer, solvers.cloudflare)) +
  std.objectValues((import '../components/tls/sjm-social.libsonnet')(kubernetesServer, namePrefix, domain)) +
  std.objectValues((import '../components/external-dns.libsonnet')(kubernetesServer, namePrefix, "cloudflare", externalDnsProviderConfig)) +
  std.objectValues((import '../components/nfs.libsonnet')(kubernetesServer, namePrefix)) +
  std.objectValues((import '../components/observability.libsonnet')(kubernetesServer, namePrefix, false, internalDomain, lokiReplicas, prometheusReplicas, observabilityRevision, false, enableObservabilityStack)) +
  std.objectValues((import '../components/discourse.libsonnet')(kubernetesServer, namePrefix, domain, discoursePgConfiguration, smtpConfiguration, installDiscourse, discourseAdminUser))
