local k = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet');
local ns = k.core.v1.namespace;
local cj = k.batch.v1.cronJob;

function(server, namePrefix, enableObservabilityStack) {
  local common = (import 'common.libsonnet')(server, namePrefix),

  namespace:
    ns.new('feeds') +
    common.linkerdInject(),

  local cs = k.core.v1.container,

  local c =
    cs.new('the-wire-feeds', 'ghcr.io/shivjm/the-wire-feeds:sha-c8b32da') +
    cs.withImagePullPolicy('IfNotPresent') +
    cs.withArgs(
      [
        '--json',
      ] + (if enableObservabilityStack then [
        '--jaeger-agent-endpoint',
        'tempo-distributed-distributor.observability.svc.cluster.local:6831',
      ] else []) + [
        'upload',
        'thewire.xml',
      ]
    ) +
    cs.withEnvFrom(k.core.v1.envFromSource.secretRef.withName('feeds-s3')) +
    cs.withEnv(
      [
        k.core.v1.envVar.new('S3_ACL', 'public-read'),
        k.core.v1.envVar.new('RUST_LOG', 'debug,the_wire_feeds=trace'),
      ]
    ),

  cronJob:
    // Every 10 minutes.
    cj.new('update-the-wire-feeds', '*/10 * * * *', [c]) +
    cj.metadata.withNamespace('feeds') +
    cj.spec.withConcurrencyPolicy('Forbid') +
    cj.spec.jobTemplate.spec.withTtlSecondsAfterFinished(600) +
    cj.spec.jobTemplate.spec.template.metadata.withAnnotations(common.linkerdInjectAnnotation(false)) +
    cj.spec.jobTemplate.spec.template.spec.withImagePullSecrets({ name: 'github-shivjm' }) +
    cj.spec.jobTemplate.spec.template.spec.withRestartPolicy('Never'),
}
