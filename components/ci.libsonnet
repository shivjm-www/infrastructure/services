local _k = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet');
local k = _k.core.v1;
local c = k.container;
local cj = _k.batch.v1.cronJob;
local argo = import 'github.com/jsonnet-libs/argo-cd-libsonnet/2.9/main.libsonnet';
local p = argo.argoproj.v1alpha1.appProject;
local rbac = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').rbac.v1;
local auth = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').authorization.v1;
local contour = (import 'github.com/jsonnet-libs/contour-libsonnet/1.22/main.libsonnet').projectcontour.v1;
local hp = contour.httpProxy;
local withGhrNs = {
  metadata+: {
    namespace: 'github-runner',
  },
};

local dailyCronSchedule = '0 1 * * *';

function(server, namePrefix, contourEnabled, gitlabRunnerS3Endpoint, gitlabRunnerS3Bucket, domain) {
  local common = (import 'common.libsonnet')(server, namePrefix),

  local ghaRunnerScaleSet(name, repo, replicas=1) =
    common.app('r-%s' % [name], 'ci-cd', 'github-runner', {
      chart: 'actions/actions-runner-controller-charts/gha-runner-scale-set',
      repoURL: 'ghcr.io',
      targetRevision: '0.8.2',
      helm: {
        values: (importstr '../files/gha-runner-scale-set-values.yaml'),
        parameters: common.helmParameters([
          ['githubConfigUrl', 'https://github.com/%s' % [repo]],
        ]),
      },
    }) +
    common.sw('2'),

  local ghDispatchCronJob(name, repository, schedule, workflowId) =
    cj.new(name, schedule, [
      c.new('dispatch-workflow', 'alpine/httpie:3.2.1') +
      c.withImagePullPolicy('IfNotPresent') +
      c.withCommand(['https']) +
      c.withArgs(
        [
          '--ignore-stdin',
          'POST',
          'https://api.github.com/repos/$(REPOSITORY)/actions/workflows/$(WORKFLOW_ID)/dispatches',
          'Accept:application/vnd.github+json',
          'Authorization:Bearer $(AUTH_TOKEN)',
          'X-GitHub-Api-Version:2022-11-28',
          'ref=main',
          'inputs:={}',
        ]
      ) +
      c.withEnvMap({
        AUTH_TOKEN:
          k.envVarSource.secretKeyRef.withKey(name) +
          k.envVarSource.secretKeyRef.withName('workflow-dispatch-auth'),
        REPOSITORY: repository,
        WORKFLOW_ID: workflowId,
      }),
    ]) +
    withGhrNs +
    cj.spec.withConcurrencyPolicy('Forbid') +
    cj.spec.jobTemplate.spec.withTtlSecondsAfterFinished(600) +
    cj.spec.jobTemplate.spec.template.metadata.withAnnotations(common.linkerdInjectAnnotation(false)) +
    cj.spec.jobTemplate.spec.template.spec.withRestartPolicy('Never'),

  proj:
    common.project('ci-cd', 'CI/CD', [
      'https://charts.gitlab.io',
      'ghcr.io',
    ], [
      'gitlab-runner',
      'github-runner',
    ]) +
    p.spec.withNamespaceResourceWhitelist([
      {
        group: '*',
        kind: '*',
      },
    ]) +
    // List taken from error messages while syncing. Pretty much undoes
    // the security of having the project in the first place.
    p.spec.withClusterResourceWhitelist([
      {
        group: '*',
        kind: 'CustomResourceDefinition',
      },
      {
        group: '*',
        kind: 'MutatingWebhookConfiguration',
      },
      {
        group: '*',
        kind: 'ValidatingWebhookConfiguration',
      },
      {
        group: '*',
        kind: 'CustomResourceDefinition',
      },
      {
        group: '*',
        kind: 'ClusterRole',
      },
      {
        group: '*',
        kind: 'ClusterRoleBinding',
      },
    ]) +
    common.sw('0'),

  glrNs:
    k.namespace.new('gitlab-runner') +
    common.linkerdInject(),

  local withGlrNs = {
    metadata+: {
      namespace: 'gitlab-runner',
    },
  },

  // This quota would be useful but it needs corresponding annotations
  // on the Linkerd containers too. The per-container limits
  // configured in gitlab-runner-config.toml seem sufficient for now.

  // glrNsQuota:
  //   k.resourceQuota.new('gitlab-runner') +
  //   withGlrNs +
  //   k.resourceQuota.spec.withHard({
  //     'requests.cpu': '2',
  //     'requests.memory': '1Gi',
  //     'limits.cpu': '4',
  //     'limits.memory': '4Gi',
  //   }),

  sa:
    k.serviceAccount.new('gitlab') +
    withGlrNs,

  role:
    rbac.role.new('gitlab-admin') +
    withGlrNs +
    rbac.role.withRules(
      auth.resourceRule.withApiGroups('') +
      auth.resourceRule.withResources('*') +
      auth.resourceRule.withVerbs('*')
    ),

  roleBinding:
    rbac.roleBinding.new('gitlab-admin') +
    withGlrNs +
    rbac.roleBinding.roleRef.withApiGroup('rbac.authorization.k8s.io') +
    rbac.roleBinding.roleRef.withKind('Role') +
    rbac.roleBinding.roleRef.withName('gitlab-admin') +
    rbac.roleBinding.withSubjects(
      rbac.subject.withKind('ServiceAccount') +
      rbac.subject.withName('gitlab') +
      rbac.subject.withNamespace('gitlab-runner')
    ),

  glrApp:
    common.app(
      'gitlab-runner',
      'ci-cd',
      'gitlab-runner',
      {
        chart: 'gitlab-runner',
        repoURL: 'https://charts.gitlab.io',
        targetRevision: '0.64.1',
        helm: {
          parameters: [
            {
              name: 'gitlabUrl',
              value: 'https://gitlab.com/',
            },
            {
              name: 'unregisterRunners',
              value: 'true',
            },
            {
              name: 'terminationGracePeriodSeconds',
              value: '3600',
            },
            {
              name: 'concurrent',
              value: '10',
            },
            {
              name: 'checkInterval',
              value: '2',
            },
            {
              name: 'rbac.create',
              value: 'true',
            },
            {
              name: 'rbac.clusterWideAccess',
              value: 'false',
            },
            {
              name: 'metrics.enabled',
              value: 'true',
            },
            {
              name: 'runners.image',
              value: 'ubuntu:16.04',
            },
            {
              name: 'runners.config',
              value: (importstr '../files/gitlab-runner-config.toml') % [std.strReplace(gitlabRunnerS3Endpoint, 'https://', ''), gitlabRunnerS3Bucket],
            },
            {
              name: 'runners.privileged',
              value: 'false',
            },
            {
              name: 'runners.secret',
              value: 'gitlab-runner-group-token',
            },
            {
              name: 'runners.pollTimeout',
              value: '180',
            },
            {
              name: 'runners.outputLimit',
              value: '4096',
            },
            {
              name: 'runners.cache.secretName',
              value: 's3access',
            },
            {
              name: 'securityContext.fsGroup',
              value: '65533',
            },
            {
              name: 'securityContext.runAsUser',
              value: '100',
            },
            {
              name: 'resources.requests.memory',
              value: '100Mi',
            },
            {
              name: 'resources.limits.memory',
              value: '500Mi',
            },
            {
              name: 'resources.requests.cpu',
              value: '1',
            },
            {
              name: 'resources.limits.cpu',
              value: '2',
            },
          ],
        },
      }
    ) +
    common.syncOptions() +
    common.syncPolicy,

  // TODO: Set quota on namespace
  ghrNs:
    k.namespace.new('github-runner') +
    common.linkerdInject(),

  ghaController:
    common.app('github-runner', 'ci-cd', 'github-runner', {
      chart: 'actions/actions-runner-controller-charts/gha-runner-scale-set-controller',
      repoURL: 'ghcr.io',
      targetRevision: '0.8.2',
      helm: {
        values: (importstr '../files/gha-runner-scale-set-controller-values.yaml'),
      },
    }) +
    common.syncOptions(false, true) +
    common.syncPolicy,

  shivjmWwwRunnerScaleSet:
    ghaRunnerScaleSet('shivjm-www', 'shivjm-www'),

  dockerArgocdRunnerScaleSet: ghaRunnerScaleSet('shivjm-da', 'shivjm/docker-argocd'),
  dockerNodeChromiumAlpineRunnerScaleSet: ghaRunnerScaleSet('shivjm-dnca', 'shivjm/docker-node-chromium-alpine'),

  dockerNodeLibvipsCj:
    ghDispatchCronJob(
      'docker-node-libvips',
      'shivjm/docker-node-libvips',
      dailyCronSchedule,
      'publish.yml'
    ),

  dockerNodeChromium:
    ghDispatchCronJob(
      'docker-node-chromium',
      'shivjm/docker-node-chromium',
      dailyCronSchedule,
      'publish.yml'
    ),
}
