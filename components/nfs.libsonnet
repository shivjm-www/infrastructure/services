local k = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').core.v1;
local withNs = {
  metadata+: {
    namespace: 'nfs',
  },
};
local sw = '-2';

function(server, namePrefix) {
  local common = (import 'common.libsonnet')(server, namePrefix),

  ns:
    k.namespace.new('nfs') +
    common.sw(sw) +
    common.linkerdInject(),

  app:
    common.app(
      'nfs',
      'nfs',
      'nfs',
      {
        chart: 'nfs-provisioner',
        repoURL: 'https://openebs.github.io/dynamic-nfs-provisioner',
        targetRevision: '0.11.0',
        helm: {
          values: (importstr '../files/dynamic-nfs-provisioner-values.yaml'),
        },
      }
    ) +
    common.sw(sw) +
    common.syncOptions() +
    common.syncPolicy,
}
