function(server, namePrefix) {
  local common = (import 'common.libsonnet')(server, namePrefix),
  local p = common.p,

  ugcProj:
    common.project(
      'ugc',
      'UGC Application',
      [
        'https://gitlab.com/shivjm-www/infrastructure/services',
        'https://charts.bitnami.com/bitnami',
      ],
      [
        'argocd',
        'contour',
        'discourse',
        'linkerd',
      ]
    ) +
    p.spec.withClusterResourceWhitelist(
      p.spec.clusterResourceWhitelist.withGroup('*') +
      p.spec.clusterResourceWhitelist.withKind('*')
    ),

  nfsProj:
    common.project(
      'nfs',
      'NFS provisioner',
      [
        'https://openebs.github.io/dynamic-nfs-provisioner',
      ],
      [
        'nfs',
      ]
    ) +
    // TODO: Lock this down.
    p.spec.withClusterResourceWhitelist(
      p.spec.clusterResourceWhitelist.withGroup('*') +
      p.spec.clusterResourceWhitelist.withKind('*')
    ),
}
