local k = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').core.v1;

function(server, namePrefix, provider, providerConfiguration) {
  local common = (import 'common.libsonnet')(server, namePrefix),

  ns:
    k.namespace.new('external-dns') +
    common.linkerdInject() +
    common.sw('-1'),

  app:
    common.app(
      'external-dns',
      'web',
      'external-dns',
      {
        chart: 'external-dns',
        repoURL: 'https://charts.bitnami.com/bitnami',
        targetRevision: '6.36.1',
        helm: {
          valuesObject: std.parseYaml(importstr '../files/external-dns-values.yaml') +
                        { provider: provider } +
                        providerConfiguration,
        },
      }
    ) +
    common.syncOptions() +
    common.syncPolicy,
}
