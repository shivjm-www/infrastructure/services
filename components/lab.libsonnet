local k = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet');
local contour = (import 'github.com/jsonnet-libs/contour-libsonnet/1.22/main.libsonnet').projectcontour.v1;
local hp = contour.httpProxy;

local core = k.core.v1;
local deployment = k.apps.v1.deployment;
local service = core.service;

local ns = 'lab';
local labels = {
  'app.kubernetes.io/name': 'lab',
};

function(server, namePrefix, devDomain) {
  local common = (import 'common.libsonnet')(server, namePrefix),

  ns:
    core.namespace.new(ns) +
    common.sw('1') +
    common.linkerdInject(),

  deploy:
    deployment.new('lab') +
    deployment.metadata.withNamespace(ns) +
    deployment.metadata.withLabels(labels) +
    common.sw('1') +
    deployment.spec.selector.withMatchLabels(labels) +
    deployment.spec.withReplicas(1) +
    deployment.spec.template.metadata.withAnnotations({
      'argocd.argoproj.io/sync-wave': '1',
    }) +
    deployment.spec.template.metadata.withLabels(labels) +
    deployment.spec.template.spec.withContainers(
      core.container.new(
        'lab',
        'registry.gitlab.com/shivjm-www/lab:aaf8f4f1b9e8c00743eb89cf5a5518ce7c705003'
      ) +
      core.container.withPorts([{
        containerPort: 8000,
      }]) +
      core.container.livenessProbe.withInitialDelaySeconds(1) +
      core.container.livenessProbe.httpGet.withPort(8000) +
      core.container.livenessProbe.httpGet.withPath('/health') +
      core.container.securityContext.withRunAsUser(65532) +
      core.container.securityContext.capabilities.withDrop(['ALL']) +
      // Fresh writes to ~/.cache and DENO_DIR at runtime. We can
      // mount an ephemeral volume for ~ but doing that for DENO_DIR
      // would undo the work of caching the application in the
      // Dockerfile itself. Existing issue:
      // https://github.com/denoland/fresh/issues/990
      core.container.withVolumeMounts([
        core.volumeMount.new('home', '/home/nonroot'),
      ])
    ) +
    deployment.spec.template.spec.withVolumes([
      core.volume.fromEmptyDir('home'),
    ]),

  service:
    service.new('lab', labels, [{
      port: 8000,
      targetPort: 8000,
    }]) +
    service.metadata.withNamespace(ns),

  hp:
    hp.new('lab') +
    hp.metadata.withNamespace(ns) +
    hp.metadata.withAnnotations(common.crdSyncOptions()) +
    common.sw('1') +
    hp.spec.withIngressClassName('contour') +
    hp.spec.virtualhost.withFqdn('lab.' + devDomain) +
    hp.spec.withRoutes(
      hp.spec.routes.withConditions(
        hp.spec.routes.conditions.withPrefix('/')
      ) +
      hp.spec.routes.withServices(
        hp.spec.routes.services.withName('lab') +
        hp.spec.routes.services.withPort(8000)
      ) +
      hp.spec.routes.withEnableWebsockets(true),
    ) +
    common.contourRateLimiting +
    common.tlsConfig,
}
