function(server, namePrefix) {
  local common = (import 'common.libsonnet')(server, namePrefix),
  local p = common.p,

  proj:
    common.project('web',
                   'Web stack',
                   [
                     'https://charts.bitnami.com/bitnami',
                   ],
                   [
                     'contour',
                     'external-dns',
                     'observability',
                   ]) +
    common.sw('-1') +
    p.spec.withClusterResourceWhitelist(std.map(
      function(v) p.spec.clusterResourceWhitelist.withGroup(v[0]) +
                  p.spec.clusterResourceWhitelist.withKind(v[1]),
      [
        ['apiextensions.k8s.io', 'CustomResourceDefinition'],
        ['networking.k8s.io', 'IngressClass'],
        ['rbac.authorization.k8s.io', 'ClusterRole'],
        ['rbac.authorization.k8s.io', 'ClusterRoleBinding'],
      ]
    )) +
    p.spec.withNamespaceResourceWhitelist(std.map(
      function(v) p.spec.clusterResourceWhitelist.withGroup(v[0]) +
                  p.spec.clusterResourceWhitelist.withKind(v[1]),
      [
        ['projectcontour.io/v1', 'TLSCertificateDelegation'],
        ['', 'ServiceAccount'],
        ['', 'Service'],
        ['', 'ConfigMap'],
        ['', 'Secret'],
        ['', 'Pod'],
        ['apps', 'ReplicaSet'],
        ['apps', 'Deployment'],
        ['apps', 'DaemonSet'],
        ['rbac.authorization.k8s.io', 'Role'],
        ['rbac.authorization.k8s.io', 'RoleBinding'],
        ['monitoring.coreos.com', 'ServiceMonitor'],
        ['networking.k8s.io', 'NetworkPolicy'],
      ]
    )),
}
