function(server, namePrefix) {
  local common = (import 'common.libsonnet')(server, namePrefix),

  app:
    common.app('cert-manager', 'linkerd', 'cert-manager', {
      chart: 'cert-manager',
      repoURL: 'https://charts.jetstack.io',
      helm: {
        values: (importstr '../files/cert-manager.yaml'),
      },
      targetRevision: '1.15.1',
    }) +
    common.sw('-6') +
    common.syncOptions(true) +
    common.syncPolicy,
}
