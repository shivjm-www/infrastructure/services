local k = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').core.v1;
local contour = (import 'github.com/jsonnet-libs/contour-libsonnet/1.22/main.libsonnet').projectcontour.v1;
local hp = contour.httpProxy;
local withNs = {
  metadata+: {
    namespace: 'observability',
  },
};
local monitoredNamespaces = [
  'argocd',
  'contour',
  'default',
  'external-dns',
  'feeds',
  'github-runner',
  'gitlab-runner',
  'kube-system',
  'linkerd',
  'linkerd-jaeger',
  'linkerd-viz',
];

function(server, namePrefix, enableContour, domain, lokiReplicas, prometheusReplicas, observabilityRevision, localS3, enableObservabilityStack) {
  local common = (import 'common.libsonnet')(server, namePrefix),
  local p = common.p,

  crds:
    common.app('kube-prometheus-crds', 'linkerd', 'linkerd', {
      repoURL: 'https://github.com/prometheus-operator/kube-prometheus.git',

      // This revision should be kept in sync with the one in the
      // `observability` Application.
      targetRevision: '90f30144c48fa495a125c0a51114a8dac8316d81',

      path: 'manifests/setup',
      directory: {
        recurse: false,
        include: '0*.yaml',
      },
    }) +
    common.sw('-6') +
    common.syncOptions(false, true, true),
} + if enableObservabilityStack then {
  local common = (import 'common.libsonnet')(server, namePrefix),
  local p = common.p,

  ns:
    k.namespace.new('observability') +
    common.sw('-3') +
    common.linkerdInject(),

  proj:
    common.project(
      'observability',
      'Observability stack deployment',
      [
        'https://gitlab.com/shivjm-www/infrastructure/observability.git',
        'git@github.com:prometheus-operator/kube-prometheus.git',
        'https://charts.enix.io',
        'https://grafana.github.io/helm-charts',
        'https://helm.elastic.co',
        'https://github.com/danielqsj/kafka_exporter.git',
        'https://prometheus-community.github.io/helm-charts',
        'https://charts.bitnami.com/bitnami',
        'https://helm.elastic.co',
        'https://jaegertracing.github.io/helm-charts',
        'https://charts.visonneau.fr',
        'https://github.com/timescale/promscale',
        'https://charts.timescale.com/',
      ],
      [
        'observability',
      ] + monitoredNamespaces,
    ) +
    common.sw('-3') +
    // TODO: Narrow this down.
    p.spec.withClusterResourceWhitelist(
      p.spec.clusterResourceWhitelist.withGroup('*') +
      p.spec.clusterResourceWhitelist.withKind('*')
    ),

  app:
    common.app(
      'observability',
      'observability',
      'observability',
      {
        directory: {
          jsonnet: {
            tlas: [
              {
                name: 'monitoredNamespaces',
                value: std.manifestJsonEx(monitoredNamespaces, '  '),
                code: true,
              },
              {
                name: 'lokiReplicas',
                value: std.toString(lokiReplicas),
                code: true,
              },
              {
                name: 'prometheusReplicas',
                value: std.toString(prometheusReplicas),
                code: true,
              },
              {
                name: 'localS3',
                value: std.toString(localS3),
                code: true,
              },
              {
                name: 'kubernetesServer',
                value: server,
              },
              {
                name: 'namePrefix',
                value: namePrefix,
              },
            ],
            libs: [
              'vendor',
              'libs',
            ],
          },
          recurse: true,
        },
        path: 'apps',
        repoURL: 'https://gitlab.com/shivjm-www/infrastructure/observability.git',
        targetRevision: observabilityRevision,
      },
      [],
      'https://kubernetes.default.svc'
    ) +
    common.sw('-1') +
    common.syncOptions() +
    common.syncPolicy,

} + (if enableContour then {
       local common = (import 'common.libsonnet')(server, namePrefix),

       grafanaRoute:
         hp.new('grafana') +
         withNs +
         hp.metadata.withAnnotations(common.crdSyncOptions()) +
         common.sw('1') +
         hp.spec.withIngressClassName('contour') +
         hp.spec.virtualhost.withFqdn('stats.' + domain) +
         hp.spec.withRoutes(
           hp.spec.routes.withConditions(
             hp.spec.routes.conditions.withPrefix('/')
           ) +
           hp.spec.routes.withServices(
             hp.spec.routes.services.withName('grafana') +
             hp.spec.routes.services.withPort(3000)
           ) +
           hp.spec.routes.withEnableWebsockets(true),
         ) +
         common.contourRateLimiting +
         common.tlsConfig,
     } else {}) else {}
