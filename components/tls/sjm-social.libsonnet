local withNs = {
  metadata+: {
    namespace: 'contour',
  },
};

function(kubernetesServer, namePrefix, domain) {
  local common = (import 'common.libsonnet')(kubernetesServer, namePrefix),
  local cm = (import 'github.com/jsonnet-libs/cert-manager-libsonnet/1.9/main.libsonnet'),
  local c = cm.nogroup.v1.certificate,
  local contour = (import 'github.com/jsonnet-libs/contour-libsonnet/1.22/main.libsonnet').projectcontour.v1,
  local cd = contour.tlsCertificateDelegation,

  cert:
    c.new('sjm-social') +
    withNs +
    c.metadata.withAnnotations({
      'cert-manager.io/issue-temporary-certificate': 'true',
    } + common.crdSyncOptions()) +
    common.sw('1') +
    c.spec.withSecretName('sjm-social-tls') +
    c.spec.withDnsNames(
      [
        domain,
      ]
    ) +
    c.spec.issuerRef.withName('letsencrypt') +
    c.spec.issuerRef.withKind('Issuer') +
    c.spec.issuerRef.withGroup('cert-manager.io') +
    c.spec.privateKey.withRotationPolicy('Always'),

  delegation:
    cd.new('sjm-social') +
    withNs +
    cd.metadata.withAnnotations(common.crdSyncOptions()) +
    common.sw('1') +
    cd.spec.withDelegations(
      cd.spec.delegations.withSecretName('sjm-social-tls') +
      cd.spec.delegations.withTargetNamespaces([
        'discourse',
      ])
    ),
}
