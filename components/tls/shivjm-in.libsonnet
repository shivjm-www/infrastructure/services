local withNs = {
  metadata+: {
    namespace: 'contour',
  },
};

function(kubernetesServer, namePrefix, domain, internalDomain, devDomain) {
  local common = (import 'common.libsonnet')(kubernetesServer, namePrefix),
  local cm = (import 'github.com/jsonnet-libs/cert-manager-libsonnet/1.9/main.libsonnet'),
  local c = cm.nogroup.v1.certificate,
  local contour = (import 'github.com/jsonnet-libs/contour-libsonnet/1.22/main.libsonnet').projectcontour.v1,
  local cd = contour.tlsCertificateDelegation,

  cert:
    c.new('shivjm-in') +
    withNs +
    c.metadata.withAnnotations({
      'cert-manager.io/issue-temporary-certificate': 'true',
    } + common.crdSyncOptions()) +
    common.sw('1') +
    c.spec.withSecretName('shivjm-in-tls') +
    c.spec.withDnsNames(
      [
        '*.' + domain,
        '*.' + internalDomain,
        '*.' + devDomain,
      ]
    ) +
    c.spec.issuerRef.withName('letsencrypt') +
    c.spec.issuerRef.withKind('Issuer') +
    c.spec.issuerRef.withGroup('cert-manager.io') +
    c.spec.privateKey.withRotationPolicy('Always'),

  delegation:
    cd.new('shivjm-in') +
    withNs +
    cd.metadata.withAnnotations(common.crdSyncOptions()) +
    common.sw('1') +
    cd.spec.withDelegations(
      cd.spec.delegations.withSecretName('shivjm-in-tls') +
      cd.spec.delegations.withTargetNamespaces([
        'argocd',
        'github-runner',
        'observability',
        'lab',
        'rustypaste',
      ])
    ),
}
