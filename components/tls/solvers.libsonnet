local cm = (import 'github.com/jsonnet-libs/cert-manager-libsonnet/1.9/main.libsonnet');
local i = cm.nogroup.v1.issuer;

{
  digitalocean:
    i.spec.acme.solvers.dns01.digitalocean.tokenSecretRef.withName('digitalocean') +
    i.spec.acme.solvers.dns01.digitalocean.tokenSecretRef.withKey('access-token'),

  cloudflare:
    i.spec.acme.solvers.dns01.cloudflare.withEmail('shiv@shivjm.in') +
    i.spec.acme.solvers.dns01.cloudflare.apiTokenSecretRef.withName('cloudflare') +
    i.spec.acme.solvers.dns01.cloudflare.apiTokenSecretRef.withKey('api-token'),
}
