local k = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet');
local contour = (import 'github.com/jsonnet-libs/contour-libsonnet/1.22/main.libsonnet').projectcontour.v1;
local hp = contour.httpProxy;

local core = k.core.v1;
local deployment = k.apps.v1.deployment;
local service = core.service;
local pvc = core.persistentVolumeClaim;

local ns = 'rustypaste';
local labels = {
  'app.kubernetes.io/name': 'rustypaste',
};

function(server, namePrefix, domain) {
  local common = (import 'common.libsonnet')(server, namePrefix),

  namespace:
    core.namespace.new(ns) +
    common.linkerdInject(),

  pvc:
    pvc.new('uploads') +
    pvc.metadata.withNamespace(ns) +
    pvc.spec.resources.withRequests({
      storage: '5Gi',
    }) +
    pvc.spec.withAccessModes(['ReadWriteOnce']),

  deploy:
    deployment.new('rustypaste') +
    deployment.metadata.withNamespace(ns) +
    deployment.metadata.withLabels(labels) +
    common.sw('1') +
    deployment.spec.selector.withMatchLabels(labels) +
    deployment.spec.withReplicas(1) +
    // The PVC can’t be shared so two Pods must not exist at the same
    // time.
    deployment.spec.strategy.withType("Recreate") +
    deployment.spec.template.metadata.withAnnotations({
      'argocd.argoproj.io/sync-wave': '1',
    }) +
    deployment.spec.template.metadata.withLabels(labels) +
    deployment.spec.template.spec.withContainers(
      core.container.new(
        'rustypaste',
        'orhunp/rustypaste:sha-15e3619'
      ) +
      core.container.withPorts([
        core.containerPort.newNamed(8000, 'http'),
      ]) +
      core.container.livenessProbe.withInitialDelaySeconds(1) +
      core.container.livenessProbe.httpGet.withPort('http') +
      core.container.livenessProbe.httpGet.withPath('/') +
      core.container.securityContext.withRunAsNonRoot(true) +
      core.container.securityContext.capabilities.withDrop(['ALL']) +
      core.container.withEnv([
        core.envVar.new('CONFIG', '/rustypaste-config/config.toml'),
      ]) +
      core.container.withVolumeMounts([
        core.volumeMount.new('uploads', '/uploads'),
        core.volumeMount.new('config-directory', '/rustypaste-config', true),
      ])
    ) +
    deployment.spec.template.spec.securityContext.withFsGroup(1000) +
    deployment.spec.template.spec.withVolumes([
      core.volume.fromPersistentVolumeClaim('uploads', 'uploads'),
      core.volume.fromSecret('config-directory', 'rustypaste'),
    ]),

  service:
    service.new('rustypaste', labels, [{
      port: 80,
      targetPort: 'http',
    }]) +
    service.metadata.withNamespace(ns),

  hp:
    hp.new('rustypaste') +
    hp.metadata.withNamespace(ns) +
    hp.metadata.withAnnotations(common.crdSyncOptions()) +
    common.sw('1') +
    hp.spec.withIngressClassName('contour') +
    hp.spec.virtualhost.withFqdn('kaagaz.' + domain) +
    hp.spec.withRoutes(
      hp.spec.routes.withConditions(
        hp.spec.routes.conditions.withPrefix('/')
      ) +
      hp.spec.routes.withServices(
        hp.spec.routes.services.withName('rustypaste') +
        hp.spec.routes.services.withPort(80)
      )
    ) +
    common.contourRateLimiting +
    common.tlsConfig,
}
