function(server, namePrefix)
  local common = (import 'common.libsonnet')(server, namePrefix);
  local project = common.project;
  local a = common.a;
  local p = common.p;

  [
    project('linkerd', 'Linkerd and identity', [
      'https://charts.jetstack.io',
      'https://helm.linkerd.io/edge',
      'https://github.com/Soluto/linkerd-disable-injection-mutation-webhook',
      'https://github.com/prometheus-operator/kube-prometheus.git',
    ], [
      'kube-system',
      'cert-manager',
      'linkerd',
      'linkerd-cni',
      'linkerd-viz',
      'linkerd-jaeger',
      'linkerd-crds',
    ]) +
    common.sw('-6') +
    p.spec.withClusterResourceWhitelist(
      p.spec.clusterResourceWhitelist.withGroup('*') +
      p.spec.clusterResourceWhitelist.withKind('*')
    ),
  ]
