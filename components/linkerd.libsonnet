local k = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').core.v1;
local cm = (import 'github.com/jsonnet-libs/cert-manager-libsonnet/1.9/main.libsonnet');
local c = cm.nogroup.v1.certificate;
local i = cm.nogroup.v1.issuer;
local revisions = {
  crds: '2024.7.3',  // https://artifacthub.io/packages/helm/linkerd2-edge/linkerd-crds
  controlPlane: '2024.7.3-edge',  // https://artifacthub.io/packages/helm/linkerd2-edge/linkerd-control-plane
  jaeger: '2024.7.3-edge',  // https://artifacthub.io/packages/helm/linkerd2-edge/linkerd-jaeger
  viz: '2024.7.3-edge',  // https://artifacthub.io/packages/helm/linkerd2-edge/linkerd-viz
  cni: '2024.7.3-edge',  // https://artifacthub.io/packages/helm/linkerd2-edge/linkerd2-cni
};
local withRepo = {
  repoURL: 'https://helm.linkerd.io/edge',
};

function(server, namePrefix, domain, internalDomain, cni, replicas, enableObservabilityStack) {
  local common = (import 'common.libsonnet')(server, namePrefix),
  local withNs = {
    metadata+: {
      namespace: 'linkerd',
    },
  },

  ns:
    k.namespace.new('linkerd') +
    common.sw('-6'),

  issuer:
    i.new('linkerd-trust-anchor') +
    withNs +
    common.sw('-5') +
    i.metadata.withAnnotationsMixin(common.crdSyncOptions()) +
    i.spec.ca.withSecretName('linkerd-trust-anchor'),

  identityCertificate:
    c.new('linkerd-identity-issuer') +
    c.metadata.withNamespace('linkerd') +
    common.sw('-5') +
    c.metadata.withAnnotationsMixin(common.crdSyncOptions()) +
    c.spec.withSecretName('linkerd-identity-issuer') +
    c.spec.withDuration('48h0m0s') +
    c.spec.withRenewBefore('25h0m0s') +
    c.spec.issuerRef.withName('linkerd-trust-anchor') +
    c.spec.issuerRef.withKind('Issuer') +
    c.spec.withCommonName('identity.linkerd.cluster.local') +
    c.spec.withDnsNames(['identity.linkerd.cluster.local']) +
    c.spec.withIsCA(true) +
    c.spec.privateKey.withAlgorithm('ECDSA') +
    c.spec.withUsages([
      'cert sign',
      'crl sign',
      'server auth',
      'client auth',
    ]),

  crds:
    common.app('linkerd-crds', 'linkerd', 'linkerd', {
      chart: 'linkerd-crds',
      targetRevision: revisions.crds,
    } + withRepo, [
      {
        group: 'apiextensions.k8s.io',
        kind: 'CustomResourceDefinition',
        jsonPointers: [
          '/spec/preserveUnknownFields',
          '/spec/names/shortNames',
        ],
      },
    ]) +
    common.sw('-4') +
    common.syncOptions() +
    common.syncPolicy,

  controlPlane:
    common.app(
      'linkerd-control-plane',
      'linkerd',
      'linkerd',
      {
        chart: 'linkerd-control-plane',
        targetRevision: revisions.controlPlane,
        helm: {
          values: (importstr '../files/linkerd-control-plane-values.yaml'),
          parameters: common.helmParameters(
            [
              ['cniEnabled', std.toString(cni)],
              ['controllerReplicas', std.toString(replicas)],
            ]
          ),
        },
      } + withRepo,
      [
        {
          group: 'apiextensions.k8s.io',
          kind: 'CustomResourceDefinition',
          jsonPointers: [
            '/spec/preserveUnknownFields',
          ],
        },
        {
          kind: 'Secret',
          name: 'linkerd-proxy-injector-k8s-tls',
          namespace: 'linkerd',
          jsonPointers: [
            '/data/tls.crt',
            '/data/tls.key',
          ],
        },
        {
          kind: 'Secret',
          name: 'linkerd-policy-validator-k8s-tls',
          namespace: 'linkerd',
          jsonPointers: [
            '/data/tls.crt',
            '/data/tls.key',
          ],
        },
        {
          kind: 'Secret',
          namespace: 'linkerd',
          name: 'linkerd-sp-validator-k8s-tls',
          jsonPointers: [
            '/data/tls.crt',
            '/data/tls.key',
          ],
        },
        {
          group: 'admissionregistration.k8s.io',
          kind: 'MutatingWebhookConfiguration',
          name: 'linkerd-proxy-injector-webhook-config',
          jsonPointers: [
            '/webhooks/0/clientConfig/caBundle',
          ],
        },
        {
          group: 'admissionregistration.k8s.io',
          kind: 'ValidatingWebhookConfiguration',
          name: 'linkerd-policy-validator-webhook-config',
          jsonPointers: [
            '/webhooks/0/clientConfig/caBundle',
          ],
        },
        {
          group: 'admissionregistration.k8s.io',
          kind: 'ValidatingWebhookConfiguration',
          name: 'linkerd-sp-validator-webhook-config',
          jsonPointers: [
            '/webhooks/0/clientConfig/caBundle',
          ],
        },
        {
          group: 'batch',
          kind: 'CronJob',
          namespace: 'linkerd',
          name: 'linkerd-heartbeat',
          jsonPointers: [
            '/spec/schedule',
          ],
        },
      ]
    ) +
    common.sw('-3') +
    common.syncOptions() +
    common.syncPolicy,

  jaeger:
    common.app(
      'linkerd-jaeger', 'linkerd', 'linkerd-jaeger', {
        chart: 'linkerd-jaeger',
        targetRevision: revisions.jaeger,
        helm: {
          values: (importstr '../files/linkerd-jaeger-values.yaml'),
        },
      } + withRepo, [
        {
          group: 'apiextensions.k8s.io',
          kind: 'CustomResourceDefinition',
          jsonPointers: [
            '/spec/preserveUnknownFields',
          ],
        },
        {
          kind: 'Secret',
          namespace: 'linkerd-jaeger',
          name: 'jaeger-injector-k8s-tls',
          jsonPointers: [
            '/data/tls.crt',
            '/data/tls.key',
          ],
        },
        {
          group: 'admissionregistration.k8s.io',
          kind: 'MutatingWebhookConfiguration',
          name: 'linkerd-jaeger-injector-webhook-config',
          jsonPointers: [
            '/webhooks/0/clientConfig/caBundle',
          ],
        },
      ]
    ) +
    common.sw('0') +
    common.syncOptions(true) +
    common.syncPolicy,

  // TODO: Add new SMI extension (implements Service Mesh Interface
  // spec) when more mature:
  // https://linkerd.io/2.15/tasks/linkerd-smi/
  // smi:
  //   common.app(
  //     'linkerd-smi',
  //     'linkerd',
  //     'linkerd-smi',
  //     {
  //       'chart': 'linkerd-smi',
  //       targetRevision: revisions.smi,
  //       helm: {}
  //     }
  //   ),
} + (if cni then {
       local common = (import 'common.libsonnet')(server, namePrefix),

       cni:
         common.app(
           'linkerd-cni',
           'linkerd',
           'linkerd',
           {
             chart: 'linkerd2-cni',
             targetRevision: revisions.cni,
           } + withRepo
         ) +
         common.sw('-4') +
         common.syncOptions() +
         common.syncPolicy,
     } else {}) + (if enableObservabilityStack then {
                     local common = (import 'common.libsonnet')(server, namePrefix),

                     vizNs:
                       k.namespace.new('linkerd-viz') +
                       common.sw('-1') +
                       k.namespace.metadata.withLabels({
                         'linkerd.io/extension': 'viz',
                       }),

                     viz:
                       common.app(
                         'linkerd-viz',
                         'linkerd',
                         'linkerd-viz',
                         {
                           chart: 'linkerd-viz',
                           targetRevision: revisions.viz,
                           helm: {
                             parameters: common.helmParameters([
                               [
                                 'grafana.enabled',
                                 'false',
                               ],
                               [
                                 'prometheus.enabled',
                                 'false',
                               ],
                               [
                                 'prometheusUrl',
                                 'http://prometheus-k8s.observability.svc.cluster.local:9090',
                               ],
                               [
                                 'grafana.externalUrl',
                                 'https://stats.%s%s' % [internalDomain, domain],
                               ],
                               // Tempo doesn’t provide a UI like Jaeger does.
                               // [
                               //   'jaegerUrl',
                               //   'jaegertracing-query.observability.svc.cluster.local:16686',
                               // ],
                             ]),
                           },
                         } + withRepo,
                         [
                           {
                             group: 'apiextensions.k8s.io',
                             kind: 'CustomResourceDefinition',
                             jsonPointers: [
                               '/spec/preserveUnknownFields',
                             ],
                           },
                           {
                             kind: 'Secret',
                             namespace: 'linkerd-viz',
                             name: 'tap-injector-k8s-tls',
                             jsonPointers: [
                               '/data/tls.crt',
                               '/data/tls.key',
                             ],
                           },
                           {
                             kind: 'Secret',
                             namespace: 'linkerd-viz',
                             name: 'tap-k8s-tls',
                             jsonPointers: [
                               '/data/tls.crt',
                               '/data/tls.key',
                             ],
                           },
                           {
                             group: 'admissionregistration.k8s.io',
                             kind: 'MutatingWebhookConfiguration',
                             name: 'linkerd-tap-injector-webhook-config',
                             jsonPointers: [
                               '/webhooks/0/clientConfig/caBundle',
                             ],
                           },
                           {
                             group: 'apiregistration.k8s.io',
                             kind: 'APIService',
                             name: 'v1alpha1.tap.linkerd.io',
                             jsonPointers: [
                               '/spec/caBundle',
                             ],
                           },
                         ]
                       ) +
                       common.sw('0') +
                       common.syncOptions() +
                       common.syncPolicy,

                   } else {})
