local k = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').core.v1;
local contour = (import 'github.com/jsonnet-libs/contour-libsonnet/1.22/main.libsonnet').projectcontour.v1;
local hp = contour.httpProxy;
local withNs = {
  metadata+: {
    namespace: 'discourse',
  },
};

function(server, namePrefix, domain, pgConfiguration, smtpConfiguration, install, discourseAdminUser) {
  local common = (import 'common.libsonnet')(server, namePrefix),

  app:
    common.app('discourse',
               'ugc',
               'discourse',
               {
                 chart: 'discourse',
                 repoURL: 'https://charts.bitnami.com/bitnami',
                 targetRevision: '12.7.0',
                 helm: {
                   values: (importstr '../files/discourse-values.yaml'),
                   parameters: common.helmParameters(
                     [
                       ['auth.username', discourseAdminUser.username],
                       ['auth.email', discourseAdminUser.email],
                       ['externalDatabase.create', std.toString(install)],
                       ['host', domain],
                       ['externalDatabase.host', pgConfiguration.host],
                       ['externalDatabase.port', pgConfiguration.port],
                       ['externalDatabase.user', pgConfiguration.username],
                       ['externalDatabase.postgresUser', pgConfiguration.username],
                       ['externalDatabase.database', pgConfiguration.database],
                       ['smtp.host', smtpConfiguration.host],
                       ['smtp.port', smtpConfiguration.port],
                       ['smtp.user', smtpConfiguration.username],
                       ['discourse.startupProbe.failureThreshold', '20'],
                       ['discourse.startupProbe.timeoutSeconds', '10'],
                       ['discourse.startupProbe.initialDelaySeconds', '240'],
                     ] +
                     if install then [
                       ['image.debug', 'true'],
                       ['discourse.readinessProbe.enabled', 'false'],
                       ['discourse.startupProbe.enabled', 'false'],
                       ['discourse.livenessProbe.enabled', 'false'],
                       ['sidekiq.readinessProbe.enabled', 'false'],
                       ['sidekiq.startupProbe.enabled', 'false'],
                       ['sidekiq.livenessProbe.enabled', 'false'],
                     ] else []
                   ),
                 },
               }) +
    common.sw('2') +
    common.syncOptions() +
    common.syncPolicy,

  route:
    hp.new('discourse') +
    hp.metadata.withNamespace('discourse') +
    hp.metadata.withAnnotationsMixin(common.crdSyncOptions()) +
    hp.spec.withIngressClassName('contour') +
    hp.spec.virtualhost.withFqdn(domain) +
    hp.spec.withRoutes(hp.spec.routes.withConditions(
      hp.spec.routes.conditions.withPrefix('/')
    ) + hp.spec.routes.withServices(
      hp.spec.routes.services.withName('ugc-discourse') +
      hp.spec.routes.services.withPort(80)
    )) +
    common.sw('2') +
    common.contourRateLimiting +
    {
      spec+: {
        virtualhost+: {
          tls+: {
            secretName: 'contour/sjm-social-tls',
          },
        },
      },
    },

  pluginScriptConfigMap:
    k.configMap.new('discourse-scripts', {
      'install-discourse-plugin.sh': (importstr '../files/install-discourse-plugin.sh'),
    }) +
    withNs +
    common.sw('1'),
}
