function(server, namePrefix, domain, devDomain, internalDomain, letsencryptServer, letsencryptSolver) {
  local k = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').core.v1,
  local rbac = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').rbac.v1,
  local batch = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').batch.v1,
  local auth = (import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet').authorization.v1,
  local common = (import 'common.libsonnet')(server, namePrefix),
  local cm = (import 'github.com/jsonnet-libs/cert-manager-libsonnet/1.9/main.libsonnet'),
  local contour = (import 'github.com/jsonnet-libs/contour-libsonnet/1.22/main.libsonnet').projectcontour.v1,
  local i = cm.nogroup.v1.issuer,
  local c = cm.nogroup.v1.certificate,

  ns:
    k.namespace.new('contour') +
    common.sw('-1') +
    common.linkerdInject(),

  local withNs = {
    metadata+: {
      namespace: 'contour',
    },
  },

  envoyIssuer:
    i.new('envoy') +
    withNs +
    i.metadata.withAnnotationsMixin(common.crdSyncOptions()) +
    i.spec.ca.withSecretName('contour-ca') +
    common.sw('-2'),

  local contourCertsDuration = '%sh0m0s' % [24 * 365],

  local mTlsCertificate = function(name) c.new('%s-mtls' % [name]) +
                                         withNs +
                                         c.metadata.withAnnotations(common.crdSyncOptions()) +
                                         common.sw('-1') +
                                         c.spec.withSecretName('%s-mtls' % [name]) +
                                         c.spec.withDuration(contourCertsDuration) +
                                         c.spec.withCommonName(name) +
                                         c.spec.withDnsNames([
                                           name,
                                           '%s.contour' % [name],
                                           '%s.contour.svc' % [name],
                                           '%s.contour.svc.cluster.local' % [name],
                                         ]) +
                                         c.spec.issuerRef.withName('envoy') +
                                         c.spec.issuerRef.withKind('Issuer') +
                                         c.spec.issuerRef.withGroup('cert-manager.io') +
                                         c.spec.withUsages([
                                           'content commitment',
                                           'data encipherment',
                                           'digital signature',
                                           'key encipherment',
                                         ]) +
                                         c.spec.privateKey.withRotationPolicy('Always'),

  envoyCertificate: mTlsCertificate('envoy'),
  contourCertificate: mTlsCertificate('contour'),

  app:
    common.app(
      'contour', 'web', 'contour', {
        chart: 'contour',
        repoURL: 'https://charts.bitnami.com/bitnami',
        targetRevision: '15.5.2',
        helm: {
          values: (importstr '../files/contour-values.yaml'),
        },
      }, [
        {
          group: 'apiextensions.k8s.io',
          kind: 'CustomResourceDefinition',
          jsonPointers: [
            '/spec/preserveUnknownFields',
          ],
        },
      ]
    ) +
    common.sw('0') +
    common.syncOptions() +
    common.syncPolicy,

  letsEncryptIssuer:
    i.new('letsencrypt') +
    withNs +
    i.metadata.withAnnotations(common.crdSyncOptions()) +
    i.spec.acme.withEmail('shiv@shivjm.in') +
    i.spec.acme.privateKeySecretRef.withName('letsencrypt') +
    i.spec.acme.withServer(letsencryptServer) +
    i.spec.acme.withSolvers([
      letsencryptSolver
    ]) +
    common.sw('-4'),
}
