local contour = (import 'github.com/jsonnet-libs/contour-libsonnet/1.22/main.libsonnet').projectcontour.v1;
local hp = contour.httpProxy;

function(server, namePrefix, internalDomain) {
  local common = (import 'common.libsonnet')(server, namePrefix),

  route:
    hp.new('argocd') +
    hp.metadata.withNamespace('argocd') +
    common.sw('1') +
    hp.metadata.withAnnotationsMixin(common.crdSyncOptions()) +
    hp.spec.withIngressClassName('contour') +
    hp.spec.virtualhost.withFqdn('argo.' + internalDomain) +
    hp.spec.withRoutes(hp.spec.routes.withConditions(
      hp.spec.routes.conditions.withPrefix('/')
    ) + hp.spec.routes.withServices(
      hp.spec.routes.services.withName('argocd-server') +
      hp.spec.routes.services.withPort(80)
    )) +
    common.contourRateLimiting +
    common.tlsConfig,
}
