#!/usr/bin/env bash

set -eux

cd /opt/bitnami/discourse

for plugin in "$@"; do
    RAILS_ENV=production bundle exec rake plugin:install repo=$plugin
done

RAILS_ENV=production bundle exec rake assets:precompile

echo Plugins installed. Restart the Discourse Pods.
