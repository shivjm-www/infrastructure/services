"""Fetch, parse, and save schema files for Kubeconform.

This turns out to partially be a reimplementation of
<https://github.com/yannh/kubeconform/blob/5e63bc5ad7bf82895bd91c19e2ee160d0ffd9964/scripts/openapi2jsonschema.py>.

"""

from __future__ import annotations

import argparse
import fnmatch, tempfile
import io
import json
from pathlib import Path
from typing import NamedTuple, Iterator, Union, Any
from zipfile import ZipFile

import requests, yaml
from loguru import logger
from attrs import asdict, define


@define(frozen=True)
class SchemaFile:
    url: str
    group: str
    kind: str


@define(frozen=True)
class WildcardSource:
    url: str
    pattern: str
    exclude_patterns: list[str] = []


Source = SchemaFile | WildcardSource


@define(frozen=True)
class Schema:
    group: str
    kind: str
    contents: str


class PatchedCLoader(yaml.CLoader):
    """A very slightly modified version of yaml.CLoader. Workaround for <https://github.com/yaml/pyyaml/issues/89>."""

    yaml_implicit_resolvers = yaml.CLoader.yaml_implicit_resolvers.copy()
    yaml_implicit_resolvers.pop("=")


parser = argparse.ArgumentParser(description="Update schema files.")
parser.add_argument(
    "-s",
    "--sources-file",
    default="./schemas.json",
    type=Path,
    help="JSON file containing source definitions.",
)
parser.add_argument(
    "output_directory",
    default="../schema",
    type=Path,
    help="Directory to save schemas to.",
)
args = parser.parse_args()


def run(details: dict[str, Source], output_directory: Path) -> None:
    """Fetch and parse all schemas listed in `details`."""
    output_directory.mkdir(parents=True, exist_ok=True)

    logger.info("Fetching and parsing schemas from {} source(s)", len(details))

    for k, v in details.items():
        if isinstance(v, WildcardSource):
            for schema in fetch_and_parse_wildcard(
                v.url, v.pattern, v.exclude_patterns
            ):
                write_schema(output_directory, schema)
        elif isinstance(v, SchemaFile):
            logger.debug("Fetching {}", v.url)
            r = requests.get(v.url, stream=True)
            r.raise_for_status()
            logger.debug("Saving schema file")
            write_schema(output_directory, Schema(v.group, v.kind, r.json()))


def fetch_and_parse_wildcard(
    url: str, pattern: str, exclusion_patterns: list[str]
) -> Iterator[Schema]:
    """Fetch and parse schemas from `url`."""
    logger.info("Fetching and parsing {} files in {}", pattern, url)
    with tempfile.TemporaryDirectory(prefix="update-schemas-") as d:
        fetch_from(url, pattern, exclusion_patterns, d)
        yield from parse_schemas(Path(d))


def fetch_from(
    url: str, pattern: str, exclusion_patterns: list[str], directory: str
) -> None:
    """Fetch the schemas from `url` and place them under `directory`. Does not strip leading directory components in the ZIP files."""
    logger.info("Fetching {} files in {}", pattern, url)
    r = requests.get(url, stream=True)
    r.raise_for_status()

    # The files are extracted under a directory named after the
    # archive, so ignore the first component.
    pattern = "*/" + pattern

    with ZipFile(io.BytesIO(r.content), "r") as zf:
        filenames = zf.namelist()
        logger.debug("{} files in archive", len(filenames))

        files_to_extract = filter(
            lambda f: fnmatch.fnmatch(f, pattern)
            and not any(fnmatch.fnmatch(f, p) for p in exclusion_patterns),
            filenames,
        )
        zf.extractall(directory, files_to_extract)
        logger.debug("Extracted files")


def parse_schemas(directory: Path) -> Iterator[Schema]:
    """Parse all files under `directory` into a tuple of group, kind, and schema."""
    logger.info("Parsing schemas under {}", directory)

    for p in directory.rglob("*.*"):
        if not p.is_file():
            continue

        logger.debug("Parsing {}", str(p))
        with p.open("r", encoding="utf-8") as f:
            yield parse_crd(f)


def parse_crd(raw: io.IOBase) -> Schema:
    """Parse a CRD into a `Schema`."""
    crd = yaml.load(raw, Loader=PatchedCLoader)
    group = crd["spec"]["group"]
    kind = crd["spec"]["names"]["kind"]

    return Schema(group, kind, crd["spec"]["versions"][0]["schema"]["openAPIV3Schema"])


def write_schema(output_directory: Path, schema: Schema) -> None:
    """Write a schema to the appropriate file."""
    p = output_directory / f"{schema.group}.{schema.kind}.json".lower()
    p.write_text(json.dumps(schema.contents, indent=2), encoding="utf-8")


def parse_sources(file: Path) -> dict[str, Source]:
    with file.open("r", encoding="utf-8") as f:
        parsed = json.load(f)

        result = {}

        for label, definition in parsed.items():
            if definition.get("enabled", True) == False:
                continue

            result[label] = parse_source(definition)

        return result


def parse_source(source: dict[str, Any]) -> Source:
    ty = source["type"]
    if ty == "wildcard":
        return WildcardSource(
            source["source"], source["globs"][0], source.get("exclude", [])
        )
    if ty == "schema":
        return SchemaFile(source["source"], source["group"], source["kind"])

    raise ValueError(f"Unknown schema type: {ty}")


if __name__ == "__main__":
    run(parse_sources(args.sources_file), args.output_directory)
