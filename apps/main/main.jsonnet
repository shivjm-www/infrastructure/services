local internalSubdomain = 'internal.';
local solvers = (import '../../components/tls/solvers.libsonnet');

local externalDnsProviderConfig = {
  digitalocean+: {
    secretName: "digitalocean"
  }
};

function(kubernetesServer, namePrefix, contourEnabled, domain, devDomain, letsencryptServer, linkerdCni, linkerdReplicas, prometheusReplicas, observabilityRevision, lokiReplicas, gitlabRunnerS3Endpoint, gitlabRunnerS3Bucket, localS3, enableObservabilityStack)
  (import '../../components/bootstrap.libsonnet')(kubernetesServer, namePrefix) +
  std.objectValues((import '../../components/cert-manager.libsonnet')(kubernetesServer, namePrefix)) +
  std.objectValues((import '../../components/linkerd.libsonnet')(kubernetesServer, namePrefix, domain, internalSubdomain, linkerdCni, linkerdReplicas, enableObservabilityStack)) +
  std.objectValues((import '../../components/observability.libsonnet')(kubernetesServer, namePrefix, contourEnabled, internalSubdomain + domain, lokiReplicas, prometheusReplicas, observabilityRevision, localS3, enableObservabilityStack)) +
  std.objectValues((import '../../components/ci.libsonnet')(kubernetesServer, namePrefix, contourEnabled, gitlabRunnerS3Endpoint, gitlabRunnerS3Bucket, internalSubdomain + domain)) +
  (if contourEnabled then
     std.objectValues((import '../../components/web.libsonnet')(kubernetesServer, namePrefix)) +
     std.objectValues((import '../../components/contour.libsonnet')(kubernetesServer, namePrefix, domain, devDomain, internalSubdomain + domain, letsencryptServer, solvers.digitalocean)) +
     std.objectValues((import '../../components/tls/shivjm-in.libsonnet')(kubernetesServer, namePrefix, domain, internalSubdomain + domain, devDomain)) +
     std.objectValues((import '../../components/external-dns.libsonnet')(kubernetesServer, namePrefix, "digitalocean", externalDnsProviderConfig)) +
     std.objectValues((import '../../components/argo-extra.libsonnet')(kubernetesServer, namePrefix, internalSubdomain + domain)) else []) +
  std.objectValues((import '../../components/feeds.libsonnet')(kubernetesServer, namePrefix, enableObservabilityStack)) +
  std.objectValues((import '../../components/lab.libsonnet')(kubernetesServer, namePrefix, devDomain)) +
  std.objectValues((import '../../components/rustypaste.libsonnet')(kubernetesServer, namePrefix, domain)) +
  std.objectValues((import '../../components/ugc.libsonnet')(kubernetesServer, namePrefix))
