local internalDomain = 'internal.';
local sieve = (import 'sieve.libsonnet');

function(kubernetesServer, namePrefix, domain, letsencryptServer, linkerdReplicas, prometheusReplicas, lokiReplicas, observabilityRevision, revision, discoursePgConfiguration, smtpConfiguration, installDiscourse, discourseAdminUser, enableObservabilityStack)
  local common = (import 'common.libsonnet')(kubernetesServer, namePrefix);

  local allResources = (import 'allUgcResources.libsonnet')(kubernetesServer, namePrefix, domain, internalDomain, letsencryptServer, linkerdReplicas, prometheusReplicas, lokiReplicas, observabilityRevision, discoursePgConfiguration, smtpConfiguration, installDiscourse, discourseAdminUser, enableObservabilityStack);

  // In order to interleave resources with Applications even when the
  // Applications have to be in a separate AoA targeted at the main
  // cluster, we dynamically generate a sub-Application per wave of
  // resources.

  local waves = std.set([
    if common.hasSyncWave(r) then r.metadata.annotations[common.syncWaveAnnotation] else "0"
    for r in sieve.withoutApplications(allResources)
  ]);

  sieve.onlyApplications(allResources) +
  [
    common.app('resources-%s' % [wave], 'ugc', 'argocd', {
      repoURL: 'https://gitlab.com/shivjm-www/infrastructure/services.git',
      path: 'apps/ugc/resources',
      directory: {
        jsonnet: {
          tlas: [
            {
              name: 'kubernetesServer',
              value: kubernetesServer,
            },
            {
              name: 'namePrefix',
              value: namePrefix,
            },
            {
              name: 'wave',
              value: wave,
            },
            {
              name: 'domain',
              value: domain,
            },
            {
              name: 'internalDomain',
              value: internalDomain,
            },
            {
              name: 'letsencryptServer',
              value: letsencryptServer,
            },
            {
              name: 'linkerdReplicas',
              value: std.toString(linkerdReplicas),
            },
            {
              name: 'observabilityRevision',
              value: observabilityRevision,
            },
            {
              name: 'prometheusReplicas',
              value: std.toString(prometheusReplicas),
            },
            {
              name: 'lokiReplicas',
              value: std.toString(lokiReplicas),
            },
            {
              name: 'discourseAdminUser',
              value: std.manifestJsonMinified(discourseAdminUser)
            }
          ],
          libs: ['vendor', 'libs'],
        },
        recurse: false,
      },
      targetRevision: revision,
    }) +
    common.sw(wave) +
    common.syncOptions() +
    common.syncPolicy
    for wave in waves
  ]
