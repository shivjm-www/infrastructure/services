// A dynamic sub-Application for use with the `ugc` Application in the parent directory.

function(kubernetesServer, namePrefix, wave, domain, internalDomain, letsencryptServer, linkerdReplicas, observabilityRevision, prometheusReplicas, lokiReplicas, discourseAdminUser)
  local c = (import 'common.libsonnet')(kubernetesServer, namePrefix);
  local sieve = (import 'sieve.libsonnet');
  local sw = c.syncWaveAnnotation;

  local allResources = sieve.withoutApplications((import 'allUgcResources.libsonnet')(kubernetesServer, namePrefix, domain, internalDomain, letsencryptServer, linkerdReplicas, prometheusReplicas, lokiReplicas, observabilityRevision, {}, {}, false, {}));

  [
    r
    for r in allResources
    if (wave == 0 && !c.hasSyncWave(r)) || (c.hasSyncWave(r) && r.metadata.annotations[sw] == wave)
  ]
